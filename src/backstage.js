import {
	increaseQuality,
	decreaseSellIn,
	setQualityToMin,
	isExpired
} from './actions';

const SELL_IN_FIRST_LIMIT = 10;
const SELL_IN_SECOND_LIMIT = 5;

export default item => {
	decreaseSellIn(item);

	if (isExpired(item)) {
		setQualityToMin(item);
		return;
	}

	increaseQuality(item);

	if (item.sellIn < SELL_IN_FIRST_LIMIT) {
		increaseQuality(item);
	}
	if (item.sellIn < SELL_IN_SECOND_LIMIT) {
		increaseQuality(item);
	}
};