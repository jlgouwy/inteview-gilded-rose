import { increaseQuality, decreaseSellIn, isExpired } from './actions';

export default item => {
	decreaseSellIn(item);
	increaseQuality(item);

	if (isExpired(item)) {
		increaseQuality(item);
	}
};