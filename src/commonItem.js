import { decreaseQuality, decreaseSellIn, isExpired } from './actions';

export default item => {
  decreaseSellIn(item);
  decreaseQuality(item);

	if (isExpired(item)) {
		decreaseQuality(item);
	}
}