import updateCommon from './commonItem';
import updateAgedBrie from './agedBrie';
import updateBackstage from './backstage';
import updateLegendary from './legendary';

const AGED_BRIE = ["Aged Brie"];
const BACKSTAGE = ["Backstage passes to a TAFKAL80ETC concert"];
const LEGENDARY = ["Sulfuras, Hand of Ragnaros"];

export const Item = class Item {
  constructor(name, sellIn, quality){
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

const updateItem = item => {
  if(AGED_BRIE.includes(item.name)) {
    updateAgedBrie(item);
  } else if(BACKSTAGE.includes(item.name)) {
    updateBackstage(item);
  } else if(LEGENDARY.includes(item.name)) {
    updateLegendary(item);
  } else {
    updateCommon(item);
  }
}

export const Shop = class Shop {
  constructor(items=[]){
    this.items = items;
  }
  updateQuality() {
    this.items.forEach(updateItem);

    return this.items;
  }
}

