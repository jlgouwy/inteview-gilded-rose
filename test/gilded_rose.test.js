import { Shop, Item } from "../src/gilded_rose"; 

describe("Gilded Rose", () => {

  describe("Common", () => {
    it("should decrease the quality twice as fast when sell by date has passed", () => {
      const gildedRose = new Shop([new Item("foo", 0, 20)]);
      const items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(18);
    })
  
    it("should decrease 1 the quality", () => {
      const gildedRose = new Shop([new Item("foo", 1, 20)]);
      const items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(19);
    })
  
    it("should not display a negative quality", () => {
      const gildedRose = new Shop([new Item("foo", 1, 0)]);
      const items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(0);
    })

  });

  describe("Aged Bried", () => {
    it("should increase quality when it gets older", () => {
      const gildedRose = new Shop([new Item("Aged Brie", 1, 0)]);
      const items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(1);
    });

    it("should increase quality twice as fast when sell by date has passed", () => {
      const gildedRose = new Shop([new Item("Aged Brie", 0, 2)]);
      const items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(4);
    })

    it("should not increase the quality more than 50", () => {
      const gildedRose = new Shop([new Item("Aged Brie", 1, 50)]);
      const items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(50);
    })
  });

  describe("Backstage", () => {
    it("should increase quality by 3 under 5 days or less ", () => {
      let gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 4, 20)]);
      let items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(23);

      gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20)]);
      items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(23);
    });

    it("should increase quality by 2 under 10 days or less ", () => {
      let gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 9, 20)]);
      let items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(22);

      gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 10, 20)]);
      items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(22);
    });

    it("should increase quality by 1 over 10 days ", () => {
      let gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 11, 20)]);
      let items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(21);
    });

    it("should drop quality to 0 after the concert ", () => {
      let gildedRose = new Shop([new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20)]);
      let items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(0);
    });
  });

  describe("Legendary", () => {
    it("should not decrease quality", () => {
      const gildedRose = new Shop([new Item("Sulfuras, Hand of Ragnaros", 1, 80)]);
      const items = gildedRose.updateQuality();
  
      expect(items[0].quality).toBe(80);
    });
  });

});
